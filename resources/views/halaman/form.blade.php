<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <form action="/home" method="post">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up From</h2>
        <p>Frist name:</p>
        <input type ="text" name ="nama">
        <p>Last name:</p>
        <input type ="text">
        <br><br>
        <label>Gender:</label>
        <br><br>
        <input type="radio" name="gender" value="L"> Laki-laki
        <br>
        <input type="radio" name="gender" value="P"> Perempuan
        <br>
        <input type="radio" name="gender" value="Other"> Other
        <br><br>
        <label>Domisili</label>
        <br><br>
        <select name="kota_domisili">
            <option value="Surabaya">Surabaya</option>
            <option value="Gresik">Gresik</option>
            <option value="Lamongan">Lamongan</option>   
        </select>
        <br><br>
        <label>Language Spoken: </label>
        <br><br>
        <input type="radio" name="Bahasa" value="Indonesia"> Bahasa Indonesia
        <br>
        <input type="radio" name="Bahasa" value="English"> English
        <br>
        <input type="radio" name="Bahasa" value="bhs_Other"> Other
        <br><br>
        <label>Bio:</label>
        <br><br>
        <textarea cols="30" rows="10"></textarea>
        <br>
        <input type ="submit" value="Sign Up">
    </form>
    </form>
</body>
</html>