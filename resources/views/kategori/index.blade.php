@extends('layout.master')

@section('judul')
Halaman table
@endsection
<table class="table">

<a href="/kategori/create" class="btn btn-succes mb-3">Tambah Data</a>
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Deskrip si</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($kategori as $key=>$item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->deskripsi}}</td>
            <td>
                
                <a href="#" class="btn btn-info btn-sm">Detail</a>
                <a href="#" class="btn btn-warning btn-sm">Edit</a>
                <a href="#" class="btn btn-danger btn-sm">Delete</a>

            </td>
        
        </tr>    
      @empty
        <tr>
            <td>Data Masih Kosong</td>
        </tr>  
      @endforelse
    </tbody>
</table>
@section('content')