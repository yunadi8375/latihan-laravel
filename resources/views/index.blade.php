@extends('layout.master')

@section('judul')
Media Online
@endsection

@section('content')
    <h2>Sosial Media Developer</h2>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h2>Benefit Join di Media Online</h2>

    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Sharing knowlege</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>

    <h2>Cara Bergabung ke Media Online</h2>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href= "form">Form Sign Up</a></li>
        <li>Selesai</li>
@endsection
